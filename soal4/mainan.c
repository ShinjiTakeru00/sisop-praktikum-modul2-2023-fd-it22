#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <string.h>
#include <unistd.h>

// fungsi buat ngatasin SIGINT
void sigint_handler(int sig) {
    printf("Terminating cron job...\n");
    exit(0);
}

// buat jalanin script bash
void njalano_skrip(char* script_path) {
    int pid = fork();
    if (pid == 0) {
        execlp(script_path, script_path, NULL);
    }
}

// nampilin error message
void waduh_error() {
    printf("Error: Invalid argumen bang,\n");
    printf("harusnya gini: cron <jam> <menit> <detik> <path_to_script>\n");
}

int main(int argc, char* argv[]) {

        signal(SIGINT, sigint_handler); 
    
    // cek argumen
    if (argc != 5) {
        waduh_error();
        return 1;
    }

    
    
    // parsing argumennya
    int jam, menit, detik;
    char* script_path;
    if (sscanf(argv[1], "%d", &jam) != 1 ||
        sscanf(argv[2], "%d", &menit) != 1 ||
        sscanf(argv[3], "%d", &detik) != 1) {
        waduh_error();
        return 1;
    }
    script_path = argv[4];

    // validity argumen
    if (jam < 0 || jam > 23 ||
        menit < 0 || menit > 59 ||
        detik < 0 || detik > 59) {
        waduh_error();
        return 1;
    }



    // menjalankan cron job
    time_t now;
    struct tm* waktu_saiki;
    while (1) {
        now = time(NULL);
        waktu_saiki = localtime(&now);
        if (waktu_saiki->tm_hour == jam &&
            waktu_saiki->tm_min == menit &&
            waktu_saiki->tm_sec == detik) {
            njalano_skrip(script_path);
        }
        sleep(1);
    }

    return 0;
}


//cc:IT22-2023
