# sisop-praktikum-modul2-2023-FD-IT22



## Anggota Kelompok IT22

Evan Darya Kusuma (5027211069)

## Nomor 1

**Soal**

Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. Berikut merupakan link download dari drive kebun binatang tersebut : https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq 
a. Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.
b. Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift       penjagaan pada hewan tersebut.
c. Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.
d. Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.

Catatan : 
untuk melakukan zip dan unzip tidak boleh menggunakan system


**Code**
```
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <wait.h>

void Hewan1(){
    pid_t child_id;
    child_id = fork();

    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        char *argv[] = {"zip", "-r", "-m", "HewanAmphibi.zip", "HewanAmphibi/", NULL};
        execv("/usr/bin/zip", argv);
    }
    else {
        pid_t child_id2;
        child_id2 = fork();

        if (child_id2 < 0){
            exit(EXIT_FAILURE);
        }
        else if (child_id2 == 0){
            char *argv[] = {"zip", "-r", "-m", "HewanAir.zip", "HewanAir/", NULL};
            execv("/usr/bin/zip", argv);
        }
        else {
            int status;
            while((wait(&status)) > 0);
        }
    }
}


void Hewan2(){
    pid_t child_id;
    child_id = fork();

    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        char *argv[] = {"zip", "-r", "-m", "HewanDarat.zip", "HewanDarat/", NULL};
        execv("/bin/zip", argv);
    }
    else {
        int status;
        while((wait(&status)) > 0);
        Hewan1();
        char *argv[] = {"rm", "-rf", "HewanDarat/", "HewanAmphibi/", "HewanAir/", "binatang.zip", NULL};
        execv("/bin/rm", argv);
    }
}


int main() {
    int wget_status = system("wget -O binatang.zip \"https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq\"");
    if (wget_status != 0) {
        fprintf(stderr, "Error: Failed to download binatang.zip.\n");
        exit(EXIT_FAILURE);
    }

    pid_t child_pid = fork();
    if (child_pid == -1) {
        perror("Error: Failed to fork");
        exit(EXIT_FAILURE);
    }
    else if (child_pid == 0) {
        char *argv[] = {"unzip", "binatang.zip", NULL};
        execv("/usr/bin/unzip", argv);
        perror("Error: Failed to execute unzip");
        exit(EXIT_FAILURE);
    }
    else {
        int status;
        while (waitpid(child_pid, &status, 0) == -1) {
            perror("Error: Failed to wait for child process");
        }
        if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
            fprintf(stderr, "Error: Child process failed to unzip binatang.zip.\n");
            exit(EXIT_FAILURE);
        }

        int move_status = system("mv *darat.jpg HewanDarat/ && mv *amphibi.jpg HewanAmphibi/ && mv *air.jpg HewanAir/");
        if (move_status != 0) {
            fprintf(stderr, "Error: Failed to move image files.\n");
            exit(EXIT_FAILURE);
        }

        system("mkdir -p HewanDarat HewanAmphibi HewanAir");
        system("ls | grep -E '(darat|amphibi|air).jpg' | shuf -n 1");

        Hewan2();
    }
    return 0;
}

```

## Nomor 3

**Soal**

Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut hanya dengan 1 Program C bernama “filter.c”
Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.
Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.  
Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.
Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/
Catatan:
Format nama file yang akan diunduh dalam zip dan isi txt formasi berupa [nama]_[tim]_[posisi]_[rating].png
Tidak boleh menggunakan system()
Tidak boleh memakai function C mkdir() ataupun rename().
Gunakan exec() dan fork().
Directory “.” dan “..” tidak termasuk yang akan dihapus.
Untuk poin d DIWAJIBKAN membuat fungsi bernama buatTim(int, int, int), dengan input 3 value integer dengan urutan bek, gelandang, dan striker.


**Code**
```
#include <dirent.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>


void filterPlayers() {
    DIR *dir;
    struct dirent *ent;
    char *MU = "Manchester United";
    if ((dir = opendir(".")) != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0) {
                continue;
            }
            if (strstr(ent->d_name, MU) == NULL) {
                remove(ent->d_name);
            }
        }
        closedir(dir);
    } else {
        perror("Error: gagal membuka directory");
        exit(EXIT_FAILURE);
    }
}

void extractZip() {
    pid_t pid = fork();
    if (pid == 0) {
        execl("/usr/bin/unzip", "/usr/bin/unzip", "players.zip", NULL);
    }
    wait(NULL);
}


void deleteZip() {
    pid_t pid = fork();
    if (pid == 0) {
        execl("/bin/rm", "/bin/rm", "players.zip", NULL);
    }
    wait(NULL);
}

void categorizePlayers() {
    DIR *dir;
    struct dirent *ent;
    char* goalkeeper = "Kiper";
    char* defender = "Bek";
    char* midfielder = "Gelandang";
    char* striker = "Penyerang";
    if ((dir = opendir(".")) != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0) {
                continue;
            }
            char* token = strtok(ent->d_name, "_");
            token = strtok(NULL, "_");
            token = strtok(NULL, "_");
            char* category;
            if (strcmp(token, "Kiper") == 0) {
                category = goalkeeper;
            } else if (strcmp(token, "Bek") == 0) {
                category = defender;
            } else if (strcmp(token, "Gelandang") == 0) {
                category = midfielder;
            } else if (strcmp(token, "Penyerang") == 0) {
                category = striker;
            } else {
                continue;
            }
            makedir(category);
            pid_t pid = fork();
            if (pid == 0) {
                char path[100];
                sprintf(path, "./%s/%s", category, ent->d_name);
                execl("/bin/mv", "/bin/mv", ent->d_name, path, NULL);
            }
            wait(NULL);
        }
        closedir(dir);
    } else {
        perror("Error: Failed to open directory");
        exit(EXIT_FAILURE);
    }
}

void makedir(char* category) {
    pid_t pid = fork();
    if (pid == 0) {
        execl("/bin/mkdir", "/bin/mkdir", category, NULL);
    }
    wait(NULL);
}

```

## Nomor 4

**Soal**

Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C karena baru dipelajari olehnya.
Untuk menambah tantangan agar membuatnya semakin terfokus, Banabil membuat beberapa ketentuan custom yang harus dia ikuti sendiri. Ketentuan tersebut berupa:
Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.
Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.
Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.
Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini berjalan dalam background dan hanya menerima satu config cron.
Bonus poin apabila CPU state minimum.
Contoh untuk run: /program \* 44 5 /home/Banabil/programcron.sh 

**Code**
```
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <string.h>
#include <unistd.h>

// fungsi buat ngatasin SIGINT
void sigint_handler(int sig) {
    printf("Anjay! dah sabi jalan...\n");
    exit(0);
}

// buat jalanin script bash
void njalano_skrip(char* script_path) {
    int pid = fork();
    if (pid == 0) {
        execlp(script_path, script_path, NULL);
    }
}

// nampilin error message
void waduh_error() {
    printf("Error: Invalid argumen bang,\n");
    printf("harusnya gini: cron <jam> <menit> <detik> <path_to_script>\n");
}

int main(int argc, char* argv[]) {

        signal(SIGINT, sigint_handler); 
    
    // cek argumen
    if (argc != 5) {
        waduh_error();
        return 1;
    }

    
    
    // parsing argumennya
    int jam, menit, detik;
    char* script_path;
    if (sscanf(argv[1], "%d", &jam) != 1 ||
        sscanf(argv[2], "%d", &menit) != 1 ||
        sscanf(argv[3], "%d", &detik) != 1) {
        waduh_error();
        return 1;
    }
    script_path = argv[4];

    // validity argumen
    if (jam < 0 || jam > 23 ||
        menit < 0 || menit > 59 ||
        detik < 0 || detik > 59) {
        waduh_error();
        return 1;
    }



    // menjalankan cron job
    time_t now;
    struct tm* waktu_saiki;
    while (1) {
        now = time(NULL);
        waktu_saiki = localtime(&now);
        if (waktu_saiki->tm_hour == jam &&
            waktu_saiki->tm_min == menit &&
            waktu_saiki->tm_sec == detik) {
            njalano_skrip(script_path);
        }
        sleep(1);
    }

    return 0;
}
```
