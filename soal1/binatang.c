#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <wait.h>

void Hewan1(){
    pid_t child_id;
    child_id = fork();

    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        char *argv[] = {"zip", "-r", "-m", "HewanAmphibi.zip", "HewanAmphibi/", NULL};
        execv("/usr/bin/zip", argv);
    }
    else {
        pid_t child_id2;
        child_id2 = fork();

        if (child_id2 < 0){
            exit(EXIT_FAILURE);
        }
        else if (child_id2 == 0){
            char *argv[] = {"zip", "-r", "-m", "HewanAir.zip", "HewanAir/", NULL};
            execv("/usr/bin/zip", argv);
        }
        else {
            int status;
            while((wait(&status)) > 0);
        }
    }
}


void Hewan2(){
    pid_t child_id;
    child_id = fork();

    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        char *argv[] = {"zip", "-r", "-m", "HewanDarat.zip", "HewanDarat/", NULL};
        execv("/bin/zip", argv);
    }
    else {
        int status;
        while((wait(&status)) > 0);
        Hewan1();
        char *argv[] = {"rm", "-rf", "HewanDarat/", "HewanAmphibi/", "HewanAir/", "binatang.zip", NULL};
        execv("/bin/rm", argv);
    }
}


int main() {
    int wget_status = system("wget -O binatang.zip \"https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq\"");
    if (wget_status != 0) {
        fprintf(stderr, "Error: Failed to download binatang.zip.\n");
        exit(EXIT_FAILURE);
    }

    pid_t child_pid = fork();
    if (child_pid == -1) {
        perror("Error: Failed to fork");
        exit(EXIT_FAILURE);
    }
    else if (child_pid == 0) {
        char *argv[] = {"unzip", "binatang.zip", NULL};
        execv("/usr/bin/unzip", argv);
        perror("Error: Failed to execute unzip");
        exit(EXIT_FAILURE);
    }
    else {
        int status;
        while (waitpid(child_pid, &status, 0) == -1) {
            perror("Error: Failed to wait for child process");
        }
        if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
            fprintf(stderr, "Error: Child process failed to unzip binatang.zip.\n");
            exit(EXIT_FAILURE);
        }

        int move_status = system("mv *darat.jpg HewanDarat/ && mv *amphibi.jpg HewanAmphibi/ && mv *air.jpg HewanAir/");
        if (move_status != 0) {
            fprintf(stderr, "Error: Failed to move image files.\n");
            exit(EXIT_FAILURE);
        }

        system("mkdir -p HewanDarat HewanAmphibi HewanAir");
        system("ls | grep -E '(darat|amphibi|air).jpg' | shuf -n 1");

        Hewan2();
    }
    return 0;
}

//revisi
