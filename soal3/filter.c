#include <dirent.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>


void filterPlayers() {
    DIR *dir;
    struct dirent *ent;
    char *MU = "Manchester United";
    if ((dir = opendir(".")) != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0) {
                continue;
            }
            if (strstr(ent->d_name, MU) == NULL) {
                remove(ent->d_name);
            }
        }
        closedir(dir);
    } else {
        perror("Error: gagal membuka directory");
        exit(EXIT_FAILURE);
    }
}

void extractZip() {
    pid_t pid = fork();
    if (pid == 0) {
        execl("/usr/bin/unzip", "/usr/bin/unzip", "players.zip", NULL);
    }
    wait(NULL);
}


void deleteZip() {
    pid_t pid = fork();
    if (pid == 0) {
        execl("/bin/rm", "/bin/rm", "players.zip", NULL);
    }
    wait(NULL);
}

void categorizePlayers() {
    DIR *dir;
    struct dirent *ent;
    char* goalkeeper = "Kiper";
    char* defender = "Bek";
    char* midfielder = "Gelandang";
    char* striker = "Penyerang";
    if ((dir = opendir(".")) != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0) {
                continue;
            }
            char* token = strtok(ent->d_name, "_");
            token = strtok(NULL, "_");
            token = strtok(NULL, "_");
            char* category;
            if (strcmp(token, "Kiper") == 0) {
                category = goalkeeper;
            } else if (strcmp(token, "Bek") == 0) {
                category = defender;
            } else if (strcmp(token, "Gelandang") == 0) {
                category = midfielder;
            } else if (strcmp(token, "Penyerang") == 0) {
                category = striker;
            } else {
                continue;
            }
            makedir(category);
            pid_t pid = fork();
            if (pid == 0) {
                char path[100];
                sprintf(path, "./%s/%s", category, ent->d_name);
                execl("/bin/mv", "/bin/mv", ent->d_name, path, NULL);
            }
            wait(NULL);
        }
        closedir(dir);
    } else {
        perror("Error: Failed to open directory");
        exit(EXIT_FAILURE);
    }
}

void makedir(char* category) {
    pid_t pid = fork();
    if (pid == 0) {
        execl("/bin/mkdir", "/bin/mkdir", category, NULL);
    }
    wait(NULL);
}


